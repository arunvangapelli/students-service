# students-service
This project contains student end points. For example POST, PUT, GET, DELETE etc.
Spring modules used: Spring boot, Spring data JPA, Spring sleuth, spring aop.

Environment:
You need Java 1.8, gradle

NOTE: In memory database like h2 is not used in this project. So, to run this mysql db table set up is required, sql folder has DDL scripts.

Steps to run the project locally:

Go to a path where you want to clone. For example: /Users/avangapelli/Documents/GitHub (mac)
Then run -> git clone https://bitbucket.org/arunvangapelli/students-service.git

Go to root of the project and build it and import.

/Users/avangapelli/Documents/GitHub/students-service -> gradle clean build eclipse

File -> import -> General -> Existing projects into workspace -> click next and browse to your project path -> finish

OR

/Users/avangapelli/Documents/GitHub/students-service -> gradle clean build

File -> import -> Gradle -> Existing Gradle Project -> click next and browse to your project path -> finish

Right click on the project and Run As -> Spring Boot App. If the service starts successfully it will run on port 3007.

NOTE:
The project depends on Config server to pull config and eureka server to register. To run independently you need to use these program arguments in Eclipse or STS from Run Configurations.
--spring.cloud.config.enabled=false --eureka.client.enabled=false

From terminal or command prompt you can run as below by going to project location

For example, cd /Users/avangapelli/Documents/GitHub/students-service 

Then run java -jar build/libs/students-service-0.0.1-SNAPSHOT.jar --spring.cloud.config.enabled=false --eureka.client.enabled=false

Open postman or any other Rest client and call an end point to make sure.


