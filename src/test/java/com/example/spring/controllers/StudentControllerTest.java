package com.example.spring.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;

import com.example.spring.domain.Student;
import com.example.spring.domain.StudentResponse;
import com.example.spring.exception.domain.InvalidDataFormatException;
import com.example.spring.service.StudentService;

@RunWith(MockitoJUnitRunner.class)
public class StudentControllerTest {

    @InjectMocks
    private StudentController studentController;
    @Mock
    private StudentService studentService;
    @Mock
    private Tracer tracer;
    @Mock
    private Span span;
    Student student = new Student();

    @Before
    public void beforeTest() {
	student.setId(1l);
	student.setFirstName("Arun");
	student.setLastName("Vangapelli");

	when(tracer.getCurrentSpan()).thenReturn(span);
    }

    @Test
    public void getStudent_SuccessTest() {

	when(studentService.getStudent(1l)).thenReturn(student);

	StudentResponse studentResponse = studentController.getStudent("123", "1");
	assertEquals(1, studentResponse.getStudent().getId().intValue());
	assertEquals("Arun", studentResponse.getStudent().getFirstName());
    }

    @Test(expected = InvalidDataFormatException.class)
    public void getStudent_studentIdInvalidTest() {

	// studentId = "abc", fails in HttpValidator.
	studentController.getStudent("123", "abc");
    }

    @Test(expected = InvalidDataFormatException.class)
    public void getStudent_studentIdNullTest() {

	// studentId = null, fails in HttpValidator.
	studentController.getStudent("123", null);
    }
}
