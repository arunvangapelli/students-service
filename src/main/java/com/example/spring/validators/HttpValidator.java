package com.example.spring.validators;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.example.spring.constants.ApplicationConstants;
import com.example.spring.exception.domain.InvalidDataFormatException;

public class HttpValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpValidator.class);

    private HttpValidator() {
        // Prevent instantiation.
    }

    public static void validate(String studentId) {
        if (!StringUtils.hasText(studentId) || !Pattern.matches("\\d+", studentId)) {
            LOGGER.error("Invalid studentId = {}", studentId);
            throw new InvalidDataFormatException(ApplicationConstants.STUDENT_ID_FIELD);
        }
    }
}
