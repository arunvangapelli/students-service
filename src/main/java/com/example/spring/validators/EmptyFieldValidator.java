package com.example.spring.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.example.spring.validators.constraints.EmptyField;

public class EmptyFieldValidator implements ConstraintValidator<EmptyField, String> {

    @Override
    public void initialize(EmptyField emptyField) {
        // nothing to initialize.
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return validate(value);
    }

    /**
     * Validates whether the value is empty and passes the validation if value is null.
     *
     * @param value field to be validated for empty check
     * @return true if the value is null or is not white space only.
     */
    private static boolean validate(String value) {

        // For NULL check use @NotNull Annotation.
        return (value == null) || !StringUtils.isWhitespace(value);
    }
}