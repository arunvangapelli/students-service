package com.example.spring.error;

import com.example.spring.domain.Operation;

public class OperationsError {

    private Operation operation;

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}
