package com.example.spring.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.spring.constants.ApplicationConstants;
import com.example.spring.domain.CourseResponse;
import com.example.spring.exception.domain.ResourceNotFoundException;

@Service
public class CourseServiceAgent {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseServiceAgent.class);

    @LoadBalanced
    @Autowired
    private RestTemplate restTemplate;

    private String url = "http://COURSES-SERVICE/example/v1/courses/{id}";

    public String getCourseById(String id) {

	ResponseEntity<CourseResponse> res = null;
	try {
	    res = restTemplate.getForEntity(url, CourseResponse.class, id);

	    // TODO Update logic for 4xx and 5xx ERRORS.
	} catch (Exception ex) {
	    LOGGER.error("error ocurred. ", ex);
	}

	if (res == null || res.getStatusCode() != HttpStatus.OK) {
	    throw new ResourceNotFoundException(ApplicationConstants.COURSE_ID_FIELD);
	}
	return "OK";
    }

}
