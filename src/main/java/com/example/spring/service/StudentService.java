package com.example.spring.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.spring.constants.ApplicationConstants;
import com.example.spring.dao.StudentDao;
import com.example.spring.domain.Student;
import com.example.spring.domain.StudentCourses;
import com.example.spring.domain.StudentResponse;
import com.example.spring.entity.StudentCoursesEntity;
import com.example.spring.entity.StudentEntity;
import com.example.spring.exception.domain.ResourceNotFoundException;
import com.example.spring.internal.CourseServiceAgent;
import com.example.spring.repository.StudentCoursesRepository;
import com.example.spring.request.PostRequestBody;
import com.example.spring.request.PutRequestBody;

@Service
public class StudentService {

    @Autowired
    private StudentDao studentDao;
    @Autowired
    private CourseServiceAgent courseServiceAgent;
    @Autowired
    private StudentCoursesRepository studentCoursesRepository;
    private ModelMapper mapper = new ModelMapper();

    public List<Student> getStudents() {
	List<StudentEntity> list = studentDao.getStudents();
	return list.stream().map(studentEntity -> mapper.map(studentEntity, Student.class))
		.collect(Collectors.toList());
    }

    public Student getStudent(Long studentId) {
	StudentEntity studentEntity = studentDao.getStudent(studentId);

	if (studentEntity == null) {
	    throw new ResourceNotFoundException(ApplicationConstants.STUDENT_ID_FIELD);
	}

	return mapper.map(studentEntity, Student.class);
    }

    public Student saveStudent(PostRequestBody postRequestBody) {
	StudentEntity studentEntity = mapper.map(postRequestBody, StudentEntity.class);

	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	studentEntity.setCreatedDate(timestamp);

	StudentEntity savedStudentEntity = studentDao.saveStudent(studentEntity);
	return mapper.map(savedStudentEntity, Student.class);
    }

    public Student updateStudent(Long studentId, PutRequestBody putRequestBody) {
	StudentEntity studentEntity = studentDao.getStudent(studentId);

	if (studentEntity == null) {
	    throw new ResourceNotFoundException(ApplicationConstants.STUDENT_ID_FIELD);
	}

	studentEntity.setFirstName(putRequestBody.getFirstName());
	studentEntity.setLastName(putRequestBody.getLastName());

	StudentEntity updatedStudentEntity = studentDao.updateStudent(studentEntity);
	return mapper.map(updatedStudentEntity, Student.class);
    }

    public void deleteStudent(Long studentId) {
	StudentEntity studentEntity = studentDao.getStudent(studentId);

	if (studentEntity == null) {
	    throw new ResourceNotFoundException(ApplicationConstants.STUDENT_ID_FIELD);
	}
	studentDao.deleteStudent(studentId);
    }

    public StudentResponse addCourse(String studentId, String courseId) {
	StudentCourses studentCourses = null;
	StudentEntity studentEntity = studentDao.getStudent(Long.valueOf(studentId));

	if (studentEntity == null) {
	    throw new ResourceNotFoundException(ApplicationConstants.STUDENT_ID_FIELD);
	}

	Student student = mapper.map(studentEntity, Student.class);

	String result = courseServiceAgent.getCourseById(courseId);
	
	if (result.equals("OK")) {
	    StudentCoursesEntity studentCoursesEntity = new StudentCoursesEntity();
	    studentCoursesEntity.setStudentId(studentId);
	    studentCoursesEntity.setCourseId(courseId);

	    studentCoursesEntity = studentCoursesRepository.save(studentCoursesEntity);
	    studentCourses = mapper.map(studentCoursesEntity, StudentCourses.class);
	}

	StudentResponse studentResponse = new StudentResponse();
	studentResponse.setStudent(student);
	studentResponse.setStudentCourses(studentCourses);
	return studentResponse;
    }
}
