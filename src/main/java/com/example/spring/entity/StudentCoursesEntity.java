package com.example.spring.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "student_courses")
@IdClass(value = MyKey.class)
public class StudentCoursesEntity implements Serializable {

    private static final long serialVersionUID = 6876163278787734940L;

    @Id
    @Column(name = "student_id")
    private String studentId;

    @Id
    @Column(name = "course_id")
    private String courseId;

    public String getStudentId() {
	return studentId;
    }

    public void setStudentId(String studentId) {
	this.studentId = studentId;
    }

    public String getCourseId() {
	return courseId;
    }

    public void setCourseId(String courseId) {
	this.courseId = courseId;
    }

}
