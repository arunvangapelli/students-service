package com.example.spring.entity;

import java.io.Serializable;

public class MyKey implements Serializable {

    private static final long serialVersionUID = -7373838837373890384L;

    private String studentId;
    private String courseId;

    public String getStudentId() {
	return studentId;
    }

    public void setStudentId(String studentId) {
	this.studentId = studentId;
    }

    public String getCourseId() {
	return courseId;
    }

    public void setCourseId(String courseId) {
	this.courseId = courseId;
    }

}
