package com.example.spring.constants;

public class ApplicationConstants {

    private ApplicationConstants() {

    }

    public static final String STUDENT_ID_FIELD = "studentId";
    public static final String COURSE_ID_FIELD = "courseId";
}
