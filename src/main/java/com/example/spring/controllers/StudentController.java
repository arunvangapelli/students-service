package com.example.spring.controllers;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.spring.domain.DeleteStudentResponse;
import com.example.spring.domain.Operation;
import com.example.spring.domain.Student;
import com.example.spring.domain.StudentResponse;
import com.example.spring.domain.StudentsResponse;
import com.example.spring.request.PostRequestBody;
import com.example.spring.request.PutRequestBody;
import com.example.spring.service.StudentService;
import com.example.spring.validators.HttpValidator;

@Controller
@RequestMapping("/example/v1/students")
public class StudentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;
    @Autowired
    private Tracer tracer;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public StudentsResponse getStudents(@RequestHeader(value = "User-ID") String userId) {
	LOGGER.info("Get students Information.");

	List<Student> list = studentService.getStudents();
	StudentsResponse studentResponse = new StudentsResponse();
	studentResponse.setStudents(list);
	studentResponse.setOperation(new Operation(tracer.getCurrentSpan().traceIdString()));
	return studentResponse;
    }

    @GetMapping(value = "/{studentId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public StudentResponse getStudent(@RequestHeader(value = "User-ID") String userId,
	    @PathVariable("studentId") String studentId) {
	LOGGER.info("Get student Information.");

	// validate studentId
	HttpValidator.validate(studentId);
	Student student = studentService.getStudent(Long.valueOf(studentId));
	StudentResponse studentResponse = new StudentResponse();
	studentResponse.setStudent(student);
	studentResponse.setOperation(new Operation(tracer.getCurrentSpan().traceIdString()));
	return studentResponse;
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public StudentResponse saveStudent(@RequestHeader(value = "User-ID") String userId,
	    @Valid @RequestBody PostRequestBody postRequestBody) {
	LOGGER.info("Save student Information.");

	Student student = studentService.saveStudent(postRequestBody);
	StudentResponse studentResponse = new StudentResponse();
	studentResponse.setStudent(student);
	studentResponse.setOperation(new Operation(tracer.getCurrentSpan().traceIdString()));
	return studentResponse;
    }

    @PutMapping(value = "/{studentId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public StudentResponse updateStudent(@RequestHeader(value = "User-ID") String userId,
	    @PathVariable("studentId") String studentId, @Valid @RequestBody PutRequestBody putRequestBody) {
	LOGGER.info("Update student Information.");

	// validate studentId
	HttpValidator.validate(studentId);
	Student student = studentService.updateStudent(Long.valueOf(studentId), putRequestBody);
	StudentResponse studentResponse = new StudentResponse();
	studentResponse.setStudent(student);
	studentResponse.setOperation(new Operation(tracer.getCurrentSpan().traceIdString()));
	return studentResponse;
    }

    @DeleteMapping(value = "/{studentId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DeleteStudentResponse deleteStudent(@RequestHeader(value = "User-ID") String userId,
	    @PathVariable("studentId") String studentId) {
	LOGGER.info("Delete student Information.");

	// validate studentId
	HttpValidator.validate(studentId);
	studentService.deleteStudent(Long.valueOf(studentId));
	DeleteStudentResponse deleteStudentResponse = new DeleteStudentResponse();
	deleteStudentResponse.setOperation(new Operation(tracer.getCurrentSpan().traceIdString()));
	return deleteStudentResponse;
    }

    @PostMapping(value = "/{studentId}/courses/{courseId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public StudentResponse addCourse(@RequestHeader(value = "User-ID") String userId,
	    @PathVariable("studentId") String studentId, @PathVariable(name = "courseId") String courseId) {
	// validate studentId
	LOGGER.info("Save student course Information.");
	HttpValidator.validate(studentId);
	StudentResponse studentResponse = studentService.addCourse(studentId, courseId);
	studentResponse.setOperation(new Operation(tracer.getCurrentSpan().traceIdString()));
	return studentResponse;
    }

}
