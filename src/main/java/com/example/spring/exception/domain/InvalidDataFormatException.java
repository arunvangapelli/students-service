package com.example.spring.exception.domain;

public class InvalidDataFormatException extends RuntimeException {

    private static final long serialVersionUID = 5551645210405005178L;

    public InvalidDataFormatException() {
    }

    public InvalidDataFormatException(String message) {
        super(message);
    }
}
