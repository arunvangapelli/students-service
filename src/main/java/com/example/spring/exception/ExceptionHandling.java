package com.example.spring.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.spring.error.ErrorHelper;
import com.example.spring.error.OperationsError;
import com.example.spring.exception.domain.InvalidDataFormatException;
import com.example.spring.exception.domain.ResourceNotFoundException;
import com.example.spring.localization.MessageHandler;

@ControllerAdvice
public class ExceptionHandling {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandling.class);
    private static final String CRITICAL_ERROR = "criticalError";
    @Autowired
    private Tracer tracer;
    @Autowired
    private ErrorHelper errorHelper;
    @Autowired
    private MessageHandler messageHandler;

    /**
     * @param request original request received
     * @param ex      exception to be handled
     * @return error response body.
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public OperationsError handleGlobalException(HttpServletRequest request, Exception ex) {
	/*
	 * For unhandled exceptions, log message would contain the String criticalError
	 * so that notifications/alerts can be sent by parsing the String criticalError.
	 * Example log message would look like the following message.
	 * LogMessage=Unhandled exception occurred, is criticalError and exception class
	 * java.lang.NullPointerException
	 */
	LOGGER.error("Unhandled exception occurred, is {} and exception {} ", CRITICAL_ERROR, ex.getClass(), ex);
	return errorHelper.errorResponse("error500", tracer.getCurrentSpan().traceIdString(),
		messageHandler.localizeErrorMessage("error500"), ex.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = InvalidDataFormatException.class)
    @ResponseBody
    public OperationsError handleInvalidDataFormatException(HttpServletRequest request, InvalidDataFormatException ex) {
	LOGGER.error("InvalidDataFormatException exception occurred ", ex);
	return errorHelper.errorResponse("error400-3", tracer.getCurrentSpan().traceIdString(),
		messageHandler.localizeErrorMessage("error400-3"), ex.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = ResourceNotFoundException.class)
    @ResponseBody
    public OperationsError handleResourceNotFoundException(HttpServletRequest request, ResourceNotFoundException ex) {
	LOGGER.error("Resource not found exception occurred ", ex);
	return errorHelper.errorResponse("error404", tracer.getCurrentSpan().traceIdString(),
		messageHandler.localizeErrorMessage("error404"), ex.getMessage());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public OperationsError handleMethodArgumentNotValidException(HttpServletRequest request,
	    MethodArgumentNotValidException ex) {
	List<String> validationErrors = new ArrayList<>();

	validationErrors.addAll(ex.getBindingResult().getAllErrors().stream()
		.map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList()));

	return errorHelper.errorResponse(validationErrors, tracer.getCurrentSpan().traceIdString());
    }
}
