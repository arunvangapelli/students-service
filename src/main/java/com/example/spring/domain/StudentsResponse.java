package com.example.spring.domain;

import java.util.List;

public class StudentsResponse {

    private List<Student> students;
    private Operation operation;

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}
