package com.example.spring.domain;

public class StudentResponse {

    private Student student;
    private StudentCourses studentCourses;
    private Operation operation;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
    
    public StudentCourses getStudentCourses() {
        return studentCourses;
    }

    public void setStudentCourses(StudentCourses studentCourses) {
        this.studentCourses = studentCourses;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}
