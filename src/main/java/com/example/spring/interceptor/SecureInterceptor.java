package com.example.spring.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class SecureInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecureInterceptor.class);
    private static final String USER_ID = "User-ID";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        String userIdHeader = request.getHeader(USER_ID);

        if (!StringUtils.isEmpty(userIdHeader)) {
            MDC.put(USER_ID, userIdHeader);
        } else {
            MDC.put(USER_ID, "USER-ID-MISSING");
            LOGGER.error("User-ID header missing");
            response.setStatus(401);
            return false;
        }

        // getQueryString() returns null if the URL doesn't contain any query string.
        LOGGER.info("Incoming Request URL = {}?{} and userId = {}", request.getRequestURL(), request.getQueryString(),
                userIdHeader);

        if (StringUtils.isEmpty(userIdHeader)) {
            LOGGER.error("User-ID header is not valid, userId = {}", userIdHeader);
            response.setStatus(401);
            return false;
        }
        return true;
    }

}
