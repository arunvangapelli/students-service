package com.example.spring.aspect;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class LogAspectBase {

    protected static final String LOG_MESSAGE = "(*LOG*)";
    protected static final String BEFORE_RUNNING_CLASS = " Before running, Interface/Class intercepted is :  ";
    protected static final String BEFORE_RUNNING_METHOD = " Before running, Method intercepted is : ";
    protected static final String METHOD_ARGS = " Arguments passed to method : ";
    protected static final String AFTER_RUNNING_CLASS = " After running, Interface/Class is :  ";
    protected static final String AFTER_RUNNING_METHOD = " After running, Method is : ";
    protected static final String AFTER_RETURN_VALUE = " After returning, the return value is : ";

    /**
     * Implementation class must provide the logger
     *
     * @return Logger instance to be used for logging
     */
    protected abstract Logger getLogger();

    /**
     * Produces log info about the class, method, arguments and return value, before
     * and after proceeding with the next advice or target method invocation, in the
     * consistent format. For cases when the arguments need to be manipulated
     * somehow, use {@link #doAround(ProceedingJoinPoint, Object[])}.
     *
     * @param joinPoint keeps the info about class, method and parameters and is
     *                  used to proceed with the target
     * @return {@link Object} returned from invoking
     *         {@link ProceedingJoinPoint#proceed()}
     * @throws Throwable from {@link ProceedingJoinPoint#proceed()}
     */
    protected final Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
	return doAround(joinPoint, joinPoint.getArgs());
    }

    /**
     * Produces log info for cases when the arguments need to be manipulated
     * somehow. The caller obtains the arguments via
     * {@link ProceedingJoinPoint#getArgs()}, changes them as needed and passes them
     * as <code>args</code>
     *
     * @param joinPoint keeps the info about class, method and parameters and is
     *                  used to proceed with the target
     * @param args      array of arguments to be logged
     * @return {@link Object} returned from invoking
     *         {@link ProceedingJoinPoint#proceed()}
     * @throws Throwable from {@link ProceedingJoinPoint#proceed()}
     */
    protected final Object doAround(ProceedingJoinPoint joinPoint, Object[] args) throws Throwable {
	final ObjectMapper om = new ObjectMapper();

	// log before invoking the target method
	if (getLogger().isTraceEnabled()) {
	    getLogger().trace(LOG_MESSAGE + BEFORE_RUNNING_CLASS + joinPoint.getSignature().getDeclaringTypeName());
	    getLogger().trace(LOG_MESSAGE + BEFORE_RUNNING_METHOD + joinPoint.getSignature().getName());
	    getLogger().trace(LOG_MESSAGE + METHOD_ARGS + om.writerWithDefaultPrettyPrinter().writeValueAsString(args));
	}

	/*
	 * Note: This method can throw any Throwable and if this happens then nothing is
	 * logged from this layer.
	 */
	final Object value = joinPoint.proceed();

	// log after invocation
	if (getLogger().isTraceEnabled()) {
	    getLogger().trace(LOG_MESSAGE + AFTER_RUNNING_CLASS + joinPoint.getSignature().getDeclaringTypeName());
	    getLogger().trace(LOG_MESSAGE + AFTER_RUNNING_METHOD + joinPoint.getSignature().getName());
	    getLogger().trace(
		    LOG_MESSAGE + AFTER_RETURN_VALUE + om.writerWithDefaultPrettyPrinter().writeValueAsString(value));
	}
	return value;
    }

    /**
     * Produces log info for cases when the arguments need to be manipulated
     * somehow. The caller obtains the arguments via
     * {@link ProceedingJoinPoint#getArgs()}, changes them as needed and passes them
     * as <code>args</code>.
     *
     * @param joinPoint keeps the info about class, method and parameters and is
     *                  used to proceed with the target
     * @param args      array of arguments to be logged
     * @return {@link Object} returned from invoking
     *         {@link ProceedingJoinPoint#proceed()}
     * @throws Throwable from {@link ProceedingJoinPoint#proceed()}
     */
    protected final Object doAroundDebug(ProceedingJoinPoint joinPoint, Object[] args) throws Throwable {

	// log before invoking the target method
	if (getLogger().isDebugEnabled()) {
	    getLogger().debug(LOG_MESSAGE + BEFORE_RUNNING_CLASS + joinPoint.getSignature().getDeclaringTypeName()
		    + ". " + BEFORE_RUNNING_METHOD + joinPoint.getSignature().getName() + ". " + METHOD_ARGS
		    + Arrays.toString(args));
	}

	/*
	 * Note: This method can throw any Throwable and if this happens then nothing is
	 * logged from this layer.
	 */
	final Object value = joinPoint.proceed();

	// log after invocation
	if (getLogger().isDebugEnabled()) {
	    getLogger().debug(LOG_MESSAGE + AFTER_RUNNING_CLASS + joinPoint.getSignature().getDeclaringTypeName() + ". "
		    + AFTER_RUNNING_METHOD + joinPoint.getSignature().getName());
	}
	return value;
    }

    /**
     * Produces log info for cases when the arguments need to be manipulated
     * somehow. The caller obtains the arguments via
     * {@link ProceedingJoinPoint#getArgs()}, changes them as needed and passes them
     * as <code>args</code>
     *
     * @param joinPoint keeps the info about class, method and parameters and is
     *                  used to proceed with the target
     * @param args      array of arguments to be logged
     * @return {@link Object} returned from invoking
     *         {@link ProceedingJoinPoint#proceed()}
     * @throws Throwable from {@link ProceedingJoinPoint#proceed()}
     */
    protected final Object doAroundError(ProceedingJoinPoint joinPoint, Object[] args) throws Throwable {
	final ObjectMapper om = new ObjectMapper();

	/*
	 * Note: Unlike doAroundDebug, pretty printer is not used here for method
	 * arguments as the handler methods contain HttpServletRequest as an argument.
	 * Since this method is used for AOP logging of ExceptionHandling methods, we
	 * get an Unhandled exception when trying to pretty print.
	 */

	// log before invoking the target method
	if (getLogger().isErrorEnabled()) {
	    getLogger().error(LOG_MESSAGE + BEFORE_RUNNING_CLASS + joinPoint.getSignature().getDeclaringTypeName()
		    + ". " + BEFORE_RUNNING_METHOD + joinPoint.getSignature().getName() + ". " + METHOD_ARGS
		    + Arrays.toString(args));
	}

	/*
	 * Note: This method can throw any Throwable and if this happens then nothing is
	 * logged from this layer.
	 */
	final Object value = joinPoint.proceed();

	// log after invocation
	if (getLogger().isErrorEnabled()) {
	    getLogger().error(LOG_MESSAGE + AFTER_RUNNING_CLASS + joinPoint.getSignature().getDeclaringTypeName() + ". "
		    + AFTER_RUNNING_METHOD + joinPoint.getSignature().getName() + ". " + AFTER_RETURN_VALUE
		    + om.writerWithDefaultPrettyPrinter().writeValueAsString(value));
	}
	return value;
    }

    /**
     * Rewrites the request argument if it is of the given class
     *
     * @param arg       the argument to be rewritten, can be null
     * @param className the class name to checked for
     * @return the given argument or the rewritten argument or null if null was
     *         passed as {@code} arg {@code}
     */
    protected Object rewriteArgument(Object arg, String className) {
	if ((arg != null) && className.equalsIgnoreCase(arg.getClass().getName())) {
	    return "Argument was removed.";
	}
	return arg;
    }
}
