package com.example.spring.aspect;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect extends LogAspectBase {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(LogAspect.class);
    
    @Override
    protected Logger getLogger() {
	return LOGGER;
    }
    
    @Around("execution(* com.example.spring.controllers.*.*(..))")
    public Object doAroundController(ProceedingJoinPoint joinPoint) throws Throwable {
        // for Controller the RequestFacade argument is removed from log
        Object[] args = Arrays.stream(joinPoint.getArgs())
                .map(arg -> rewriteArgument(arg, "org.apache.catalina.connector.RequestFacade"))
                .collect(Collectors.toList()).toArray();
        return doAroundDebug(joinPoint, args);
    }

    @Around("execution(* com.example.spring.service.*.*(..))")
    public Object doAroundService(ProceedingJoinPoint joinPoint) throws Throwable {
        return doAroundDebug(joinPoint, joinPoint.getArgs());
    }

    @Around("execution(* com.example.spring.dao.*.*(..))")
    public Object doAroundResourceHelper(ProceedingJoinPoint joinPoint) throws Throwable {
        return doAroundDebug(joinPoint, joinPoint.getArgs());
    }

    @Around("execution(* com.example.spring.exception.*.*(..)) ")
    public Object doAroundException(ProceedingJoinPoint joinPoint) throws Throwable {
        return doAroundError(joinPoint, joinPoint.getArgs());
    }

}
