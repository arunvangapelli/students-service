package com.example.spring.request;

import javax.validation.constraints.NotNull;

import com.example.spring.validators.constraints.EmptyField;


public class PutRequestBody {
    
    @NotNull(message = "error400-1|firstName")
    private String firstName;
    @NotNull(message = "error400-1|lastName")
    @EmptyField(message = "error400-2|lastName")
    private String lastName;
    
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
