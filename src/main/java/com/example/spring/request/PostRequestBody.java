package com.example.spring.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class PostRequestBody {

    @NotNull(message = "error400-1|studentId")
    private String studentId;
    @NotNull(message = "error400-1|firstName")
    private String firstName;
    @NotNull(message = "error400-1|lastName")
    private String lastName;
    @NotNull(message = "error400-1|university")
    private String university;
    private Character gender;
    @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}", message = "2001-dateOfBirth")
    private String dateOfBirth;
    @NotNull(message = "error400-1|email")
    private String email;
    private String phoneNumber;
    private String address;

    public String getStudentId() {
	return studentId;
    }

    public void setStudentId(String studentId) {
	this.studentId = studentId;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getUniversity() {
	return university;
    }

    public void setUniversity(String university) {
	this.university = university;
    }

    public Character getGender() {
	return gender;
    }

    public void setGender(Character gender) {
	this.gender = gender;
    }

    public String getDateOfBirth() {
	return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getPhoneNumber() {
	return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address;
    }

}
