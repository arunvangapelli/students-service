package com.example.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.spring.entity.StudentCoursesEntity;

@Repository
public interface StudentCoursesRepository extends CrudRepository<StudentCoursesEntity, Long> {

}
