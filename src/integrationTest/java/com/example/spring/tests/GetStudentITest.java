package com.example.spring.tests;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.spring.SpringBootDemoApplication;

import io.restassured.RestAssured;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootDemoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("itest")
public class GetStudentITest {

    @LocalServerPort
    private int port;
    private String baseUrl = "/example/v1/students/{studentId}";

    @Before
    public void setup() {
	RestAssured.port = port;
	RestAssured.basePath = baseUrl;
    }

    @Test
    @Sql(scripts = "classpath:success.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "classpath:delete.sql", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
    public void getStudentSuccessTest() {
	RestAssured.given().when().header("User-ID", "123").pathParam("studentId", "1").get().then()
		.statusCode(HttpStatus.SC_OK).body("student.id", notNullValue())
		.body("student.studentId", equalTo("1234")).body("student.firstName", equalTo("Arun"))
		.body("student.lastName", equalTo("Vangapelli")).body("student.university", equalTo("University1"))
		.body("student.gender", equalTo("M")).body("student.dateOfBirth", equalTo("2000-01-01"))
		.body("student.createdDate", notNullValue()).body("student.email", equalTo("arun@email.com"))
		.body("student.phoneNumber", equalTo("123-456-789")).body("student.address", equalTo("123 Street"))
		.body("operation.correlationId", notNullValue()).body("operation.errors", nullValue());
    }

    @Test
    public void getStudent404Test() {
	//SQL scripts are not set up, so not found error.
	RestAssured.given().when().header("User-ID", "123").pathParam("studentId", "1").get().then()
		.statusCode(HttpStatus.SC_NOT_FOUND).body("operation.correlationId", notNullValue())
		.body("operation.errors[0].code", equalTo("error404"))
		.body("operation.errors[0].message", equalTo("The requested resource was not found."))
		.body("operation.errors[0].field", equalTo("studentId"));
    }

}
